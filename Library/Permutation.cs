﻿using System;

class Permutation {
    public static bool Next<T>(T[] u, Func<T, T, bool> less) {
        if (u.Length <= 1) return false;
        int k = u.Length - 2;
        while (k != -1 && !less(u[k], u[k + 1])) --k;
        if (k == -1) {
            Array.Reverse(u);
            return false;
        }
        int k1 = u.Length - 1;
        while (!less(u[k], u[k1])) --k1;
        T uk = u[k];
        u[k] = u[k1];
        u[k1] = uk;
        Array.Reverse(u, k + 1, u.Length - k - 1);
        return true;
    }

    public static bool Prev<T>(T[] u, Func<T, T, bool> less) {
        return Next(u, (k1, k2) => less(k2, k1));
    }
}
