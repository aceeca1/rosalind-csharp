using System;
using System.Collections.Generic;
using System.Linq;

class ReverseDistance {
    public class Node {
        public int[] A = new int[10];
        public int N = 0, S, T;
        public Node Parent;

        public static Node Read() {
            var ret = new Node();
            var line = Console.ReadLine().Split();
            for (int i = 0; i < 10; ++i) ret.A[i] = int.Parse(line[i]);
            return ret;
        }

        public IEnumerable<Node> Childs(int label) {
            for (int i = 0; i < 10; ++i)
                for (int j = i + 1; j < 10; ++j) {
                    var newNode = new Node() {
                        A = (int[])A.Clone(),
                        N = N + label,
                        S = i, T = j,
                        Parent = this
                    };
                    Array.Reverse(newNode.A, i, j - i + 1);
                    yield return newNode;
                }
        }

        public ulong LongHash() => Hash.Ordered(A.Select(k => (ulong)k));
    }

    public class Op {
        public int S, T;
    }
    
    public int Result = 0;
    public Op[] Ops;
    Dictionary<ulong, Node> Visited = new Dictionary<ulong, Node>();
    Node Perm1, Perm2;
    
    void ComputeOps(Node n1, Node n2) {
        Ops = new Op[Result];
        var mid = Math.Abs(n1.N);
        for (int i = mid - 1; i >= 0; --i) {
            Ops[i] = new Op { S = n1.S, T = n1.T };
            n1 = n1.Parent;
        }
        for (int i = mid; i < Ops.Length; ++i) {
            Ops[i] = new Op { S = n2.S, T = n2.T };
            n2 = n2.Parent;
        }
        if (n1 != Perm1) Array.Reverse(Ops);
    }
    
    IEnumerable<int> Search(Node perm, int label) {
        var q = new Queue<Node>();
        q.Enqueue(perm);
        int distance = 0;
        while (q.Count != 0) {
            var qH = q.Dequeue();
            if (qH.N != distance) yield return distance = qH.N;
            foreach (var i in qH.Childs(label)) {
                var h = i.LongHash();
                if (!Visited.TryGetValue(h, out Node u)) {
                    q.Enqueue(i);
                    Visited[h] = i;
                } else if (Math.Sign(u.N) == label) continue;
                else {
                    Result = Math.Abs(i.N) + Math.Abs(u.N);
                    ComputeOps(i, u);
                    throw new Success();
                }
            }
        }
    }

    public ReverseDistance(Node perm1, Node perm2) {
        Perm1 = perm1;
        Perm2 = perm2;
        if (Perm1.LongHash() == Perm2.LongHash()) return;
        var search1 = Search(Perm1,  1).GetEnumerator();
        var search2 = Search(Perm2, -1).GetEnumerator();
        try {
            for (;;) {
                search1.MoveNext();
                search2.MoveNext();
            }
        } catch (Success) {}
    }
}
