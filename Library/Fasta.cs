﻿using System.Collections.Generic;
using System.IO;
using System.Text;

class Fasta {
    public string Name;
    public string Content;

    public static IEnumerable<Fasta> Parse(TextReader text) {
        var name = text.ReadLine().Substring(1);
        var content = new StringBuilder();
        while (name != null) {
            for (;;) {
                var line = text.ReadLine();
                if (line == null || line.StartsWith(">")) {
                    yield return new Fasta {
                        Name = name,
                        Content = content.ToString()
                    };
                    name = line?.Substring(1);
                    content.Clear();
                    break;
                } else {
                    content.Append(line);
                }
            }
        }
    }
}
