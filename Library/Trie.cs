﻿using System;

class Trie {
    public static int DNANum(char c) {
        switch (c) {
            case 'A': return 0;
            case 'C': return 1;
            case 'G': return 2;
            case 'T': return 3;
            case '$': return 4;
            default: throw new ArgumentException();
        }
    }

    public class Node {
        public Node[] Childs = new Node[5];

        public Node MakeChild(int no) {
            if (Childs[no] == null) Childs[no] = new Node();
            return Childs[no];
        }
    }

    public Node Root = new Node();

    public void Add(string s) {
        var v = Root;
        foreach (var i in s) v = v.MakeChild(DNANum(i));
    }
}
