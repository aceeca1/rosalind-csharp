﻿using System.Collections.Generic;

class MinkowskiDifference {
    public static IEnumerable<long> Of(IEnumerable<long> s1, IEnumerable<long> s2) {
        foreach (var i1 in s1)
            foreach (var i2 in s2)
                yield return i1 - i2;
    }
}
