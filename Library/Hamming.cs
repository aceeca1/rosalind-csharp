﻿class Hamming {
    public static int Distance(string s1, string s2) {
        int ans = 0;
        for (int i = 0; i < s1.Length; ++i)
            if (s1[i] != s2[i]) ++ans;
        return ans;
    }
}
