using System;
using System.Collections.Generic;

class Newick {
    public class Node {
        public string Name;
        public int Parent, Weight = 1;
        public List<int> Childs = new List<int>();
    }

    public List<Node> Nodes = new List<Node>();

    public class Parser {
        public string S;
        public Newick Result = new Newick();

        int Pos = 0;

        public Parser(string s) {
            S = s;
            ParseTree();
        }

        int ParseTree() {
            Result.Nodes.Add(new Node());
            var ret = Result.Nodes.Count - 1;
            if (S[Pos] == '(') {
                ++Pos;
                for (;;) {
                    var c = ParseTree();
                    Result.Nodes[ret].Childs.Add(c);
                    Result.Nodes[c].Parent = ret;
                    if (S[Pos] != ',') break;
                    ++Pos;
                }
                ++Pos;
            }
            var nameEnd = Pos;
            while (Char.IsLetter(S[nameEnd]) || S[nameEnd] == '_') ++nameEnd;
            if (nameEnd != Pos) {
                Result.Nodes[ret].Name = S.Substring(Pos, nameEnd - Pos);
                Pos = nameEnd;
            }
            if (S[Pos] == ':') {
                ++Pos;
                var numEnd = Pos;
                while (Char.IsDigit(S[numEnd])) ++numEnd;
                var w = int.Parse(S.Substring(Pos, numEnd - Pos));
                Result.Nodes[ret].Weight = w;
                Pos = numEnd;
            }
            return ret;
        }
    }
}
