﻿using System;

class ReverseComplement {
    public static string Of(string alphabet, string s) {
        var u = new char[256];
        for (int i = 0; i < alphabet.Length; ++i)
            u[alphabet[i]] = alphabet[alphabet.Length - 1 - i];
        var ret = s.ToCharArray();
        Array.Reverse(ret);
        for (int i = 0; i < ret.Length; ++i) ret[i] = u[ret[i]];
        return new string(ret);
    }
}
