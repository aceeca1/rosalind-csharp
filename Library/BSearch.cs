﻿using System;

class BSearch {
    public static int Int(int s, int t, Func<int, bool> f) {
        for (;;) {
            if (s == t) return s;
            int m = s + ((t - s) >> 1);
            if (f(m)) t = m; else s = m + 1;
        }
    }
}
