﻿using System.Collections.Generic;
using System.IO;
using System.Net;

class Uniprot {
    public static IEnumerable<Fasta> Get(string id) {
        var url = $"http://www.uniprot.org/uniprot/{id}.fasta";
        var res = WebRequest.Create(url).GetResponse();
        var stream = res.GetResponseStream();
        var reader = new StreamReader(stream);
        return Fasta.Parse(reader);
    }
}
