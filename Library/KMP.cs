﻿using System.Collections.Generic;

class KMP {
    public string Needle;
    public int[] Next;

    public KMP(string needle) {
        Needle = needle;
        Next = new int[Needle.Length + 1];
        int k = Next[0] = -1;
        for (int i = 0; i < Needle.Length; ++i) {
            while (k >= 0 && Needle[k] != Needle[i]) k = Next[k];
            Next[i + 1] = ++k;
        }
    }

    public IEnumerable<int> Process(string haystack) {
        int k = 0;
        for (int i = 0; i < haystack.Length; ++i) {
            while (k >= 0 && Needle[k] != haystack[i]) k = Next[k];
            yield return ++k;
            if (k == Needle.Length) k = Next[k];
        }
    }
}
