﻿using System;

class Hamilton {
    public interface IGraph {
        int SizeV();
        bool E(int no1, int no2);
    }
    
    public IGraph G;
    public int[] Result;

    void Swap(int a1, int a2) {
        int t = Result[a1];
        Result[a1] = Result[a2];
        Result[a2] = t;
    }

    void Put(int no) {
        if (no == Result.Length) throw new Success();
        for (int i = no; i < Result.Length; ++i) {
            if (no != 0 && !G.E(Result[no - 1], Result[i])) continue;
            Swap(no, i);
            Put(no + 1);
            Swap(no, i);
        }
    }

    public Hamilton(IGraph g) {
        G = g;
        Result = new int[G.SizeV()];
        for (int i = 0; i < G.SizeV(); ++i) Result[i] = i;
        try {
            Put(0);
            Result = null;
        } catch (Success) {}
    }
}
