﻿using System.Collections.Generic;

class NewickDistance {
    public Newick Ne;
    Dictionary<string, int> ByName = new Dictionary<string, int>();
    int[] Depth;

    public NewickDistance(Newick ne) {
        Ne = ne;
        Depth = new int[Ne.Nodes.Count];
        for (int i = 0; i < Ne.Nodes.Count; ++i) {
            if (Ne.Nodes[i].Name != null)
                ByName.Add(Ne.Nodes[i].Name, i);
            if (i != 0) Depth[i] = Depth[Ne.Nodes[i].Parent] + 1;
        }
    }

    public int Of(string s1, string s2) {
        var i1 = ByName[s1];
        var i2 = ByName[s2];
        int ans = 0;
        for (;;) {
            if (i1 == i2) return ans;
            if (Depth[i1] < Depth[i2]) {
                ans += Ne.Nodes[i2].Weight;
                i2 = Ne.Nodes[i2].Parent;
            } else {
                ans += Ne.Nodes[i1].Weight;
                i1 = Ne.Nodes[i1].Parent;
            }
        }
    }
}
