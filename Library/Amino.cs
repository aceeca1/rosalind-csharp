﻿using System;
using System.Collections.Generic;

class Amino {
    public static double[] Mass = new double[256];
    public const int M = 10000;
    public static Dictionary<long, char> FromMass = 
        new Dictionary<long, char>();
    
    static Amino() {
        Mass['A'] = 71.03711;
        Mass['C'] = 103.00919;
        Mass['D'] = 115.02694;
        Mass['E'] = 129.04259;
        Mass['F'] = 147.06841;
        Mass['G'] = 57.02146;
        Mass['H'] = 137.05891;
        Mass['I'] = 113.08406;
        Mass['K'] = 128.09496;
        Mass['L'] = 113.08406;
        Mass['M'] = 131.04049;
        Mass['N'] = 114.04293;
        Mass['P'] = 97.05276;
        Mass['Q'] = 128.05858;
        Mass['R'] = 156.10111;
        Mass['S'] = 87.03203;
        Mass['T'] = 101.04768;
        Mass['V'] = 99.06841;
        Mass['W'] = 186.07931;
        Mass['Y'] = 163.06333;
        for (char i = 'A'; i <= 'Z'; ++i)
            if (Mass[i] != 0.0) FromMass[(long)Math.Round(M * Mass[i])] = i;
    }

    public const string CodonA = 
        "FFLLSSSSYYZZCCZWLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG";

    static int RNANum(char c) {
        switch (c) {
            case 'U': return 0;
            case 'C': return 1;
            case 'A': return 2;
            case 'G': return 3;
            default: throw new ArgumentException();
        }
    }

    public static char FromCodon(string s) {
        var a1 = RNANum(s[0]) << 4;
        var a2 = RNANum(s[1]) << 2;
        var a3 = RNANum(s[2]);
        return CodonA[a1 + a2 + a3];
    }
}
