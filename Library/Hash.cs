﻿using System.Collections.Generic;

class Hash {
    public static ulong Ordered(IEnumerable<ulong> u) {
        const ulong kMul = 0x9ddfea08eb382d69L;
        ulong ret = 0L;
        foreach (var i in u) {
            ret = (ret ^ (ulong)i) * kMul;
            ret ^= ret >> 47;
        }
        return ret * kMul;
    }
}
