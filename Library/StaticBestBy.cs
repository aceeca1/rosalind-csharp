using System;
using System.Collections.Generic;

static class StaticBestBy {
    static T BestBy<T, U>(
        IEnumerable<T> a, 
        Func<T, U> f, 
        Func<U, U, bool> better
    ) {
        var aIt = a.GetEnumerator();
        aIt.MoveNext();
        var arg = aIt.Current;
        var farg = f(arg);
        while (aIt.MoveNext()) {
            var c = aIt.Current;
            var fc = f(c);
            if (better(fc, farg)) {
                farg = fc;
                arg = c;
            }
        }
        return arg;
    }
    
    public static T MinBy<T>(this IEnumerable<T> a, Func<T, int> f) =>
        BestBy(a, f, (k1, k2) => k1 < k2);
        
    public static T MaxBy<T>(this IEnumerable<T> a, Func<T, int> f) =>
        BestBy(a, f, (k1, k2) => k1 > k2);

    public static T MinBy<T>(this IEnumerable<T> a, Func<T, double> f) =>
        BestBy(a, f, (k1, k2) => k1 < k2);
        
    public static T MaxBy<T>(this IEnumerable<T> a, Func<T, double> f) =>
        BestBy(a, f, (k1, k2) => k1 > k2);
}
