﻿using System;

class NoncrossingMatching {
    static char Complement(char c) {
        switch (c) {
            case 'A': return 'U';
            case 'C': return 'G';
            case 'G': return 'C';
            case 'U': return 'A';
            default: throw new ArgumentException();
        }
    }

    public static int Num(string s, bool perfect) {
        var a = new int[s.Length][];
        for (int i = s.Length - 1; i >= 0; --i) {
            a[i] = new int[s.Length - i];
            for (int j = 0; j < a[i].Length; ++j) {
                // a[i][j]: [i .. i + j] inclusive.
                if (!perfect) a[i][j] = j != 0 ? a[i + 1][j - 1] : 1;
                var sic = Complement(s[i]);
                for (int k = 1; k <= j; ++k)
                    if (s[i + k] == sic) {
                        var a1 = k > 1 ? a[i + 1][k - 2] : 1;
                        var a2 = k < j ? a[i + k + 1][j - k - 1] : 1;
                        a[i][j] = (int)((a[i][j] + (long)a1 * a2) % 1000000);
                    }
            }
        }
        return a[0][s.Length - 1];
    }
}
