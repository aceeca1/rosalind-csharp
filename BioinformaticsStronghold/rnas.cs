﻿using System;
using System.Numerics;

class rnas {
    static bool CanPair(char c1, char c2) {
        switch (c1) {
            case 'A': return c2 == 'U';
            case 'C': return c2 == 'G';
            case 'G': return c2 == 'C' || c2 == 'U';
            case 'U': return c2 == 'A' || c2 == 'G';
            default: throw new ArgumentException();
        }
    }

    public static void Main() {
        var s = Console.ReadLine();
        var a = new BigInteger[s.Length][];
        for (int i = s.Length - 1; i >= 0; --i) {
            a[i] = new BigInteger[s.Length - i];
            for (int j = 0; j < a[i].Length; ++j) {
                // a[i][j]: [i .. i + j] inclusive.
                a[i][j] = j != 0 ? a[i + 1][j - 1] : 1;
                for (int k = 4; k <= j; ++k)
                    if (CanPair(s[i], s[i + k])) {
                        var a1 = k > 1 ? a[i + 1][k - 2] : 1;
                        var a2 = k < j ? a[i + k + 1][j - k - 1] : 1;
                        a[i][j] += a1 * a2;
                    }
            }
        }
        Console.WriteLine(a[0][s.Length - 1]);
    }
}
