﻿using System;
using System.Collections.Generic;
using System.Linq;

class lcsm {
    static IEnumerable<uint> Slide(int k, string d) {
        if (k > d.Length) yield break;
        const uint dd = 0xdeadbeefU;
        uint ddk = 1U, hash = 0U;
        for (int i = 0; i < k; ++i) {
            hash = hash * dd + d[i];
            ddk *= dd;
        }
        for (int i = k; i < d.Length; ++i) {
            yield return hash;
            hash = hash * dd + d[i] - d[i - k] * ddk;
        }
        yield return hash;
    }

    static IEnumerable<uint> CommonSeq(List<string> dna, int k) {
        return dna.Select(u => Slide(k, u)).Aggregate(Enumerable.Intersect);
    }

    public static void Main() {
        var fasta = Fasta.Parse(Console.In);
        var dna = new List<string>(fasta.Select(k => k.Content));
        var minLen = dna.Select(k => k.Length).Min();
        var lcsLen = BSearch.Int(1, minLen + 1, k => !CommonSeq(dna, k).Any()) - 1;
        var hash = CommonSeq(dna, lcsLen).First();
        var start = Slide(lcsLen, dna[0]).TakeWhile(k => k != hash).Count();
        Console.WriteLine(dna[0].Substring(start, lcsLen));
    }
}
