using System;

class sort {
    public static void Main() {
        var perm1 = ReverseDistance.Node.Read();
        var perm2 = ReverseDistance.Node.Read();
        var rd = new ReverseDistance(perm1, perm2);
        Console.WriteLine(rd.Result);
        foreach (var i in rd.Ops) {
            Console.Write(i.S + 1);
            Console.Write(' ');
            Console.WriteLine(i.T + 1);
        }
    }
}
