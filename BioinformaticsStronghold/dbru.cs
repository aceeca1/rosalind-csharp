﻿using System;
using System.Collections.Generic;
using System.Linq;

class dbru {
    static IEnumerable<string> ReadAll() {
        for (;;) {
            var s = Console.ReadLine();
            if (s == null) break;
            yield return s;
        }
    }
    
    public static void Main() {
        var s = new HashSet<string>(ReadAll());
        var sRC = s.Select(k => ReverseComplement.Of("ACGT", k));
        s.UnionWith(new List<string>(sRC));
        foreach (var i in s) {
            var i1 = i.Substring(0, i.Length - 1);
            var i2 = i.Substring(1);
            Console.WriteLine($"({i1}, {i2})");
        }
    }
}
