﻿using System;
using System.Linq;

class prob {
    public static void Main() {
        var s = Console.ReadLine();
        var zCG = s.Count(k => k == 'C' || k == 'G');
        var zAT = s.Length - zCG;
        var ans = Console.ReadLine().Split().Select(i => {
            var n = double.Parse(i);
            var logProbCG = Math.Log10(0.5 * n);
            var logProbAT = Math.Log10(0.5 * (1.0 - n));
            return logProbCG * zCG + logProbAT * zAT;
        });
        Console.WriteLine(String.Join(" ", ans));
    }
}