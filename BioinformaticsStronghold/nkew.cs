﻿using System;
using System.Collections.Generic;

class nkew {
    static IEnumerable<int> SolveAll() {
        for (;;) {
            var s = Console.ReadLine();
            if (s == null) break;
            var newick = new Newick.Parser(s).Result;
            var name = Console.ReadLine().Split();
            yield return new NewickDistance(newick).Of(name[0], name[1]);
            if (Console.ReadLine() == null) break;
        }
    }

    public static void Main() {
        Console.WriteLine(String.Join(" ", SolveAll()));
    }
}
