﻿using System;
using System.Linq;
using System.Collections.Generic;

class fib {
    static IEnumerable<long> Fibonacci(int k) {
        long a0 = 1L, a1 = 1L;
        for (;;) {
            yield return a0;
            long a2 = a1 + k * a0;
            a0 = a1;
            a1 = a2;
        }
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        Console.WriteLine(Fibonacci(k).ElementAt(n - 1));
    }
}
