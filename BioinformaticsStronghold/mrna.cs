﻿using System;
using System.Linq;

class mrna {
    public static void Main() {
        var times = new int[256];
        foreach (var i in Amino.CodonA) ++times[i];
        var a = (Console.ReadLine() + "Z").Select(k => times[k]);
        Console.WriteLine(a.Aggregate((k1, k2) => k1 * k2 % 1000000));
    }
}
