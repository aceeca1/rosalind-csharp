﻿using System;
using System.Collections.Generic;
using System.Linq;

class prsm {
    const int M = 100000;

    static IEnumerable<double> CompleteSpectrum(string s) {
        double ans = 0.0;
        for (int i = 0; i < s.Length; ++i) {
            ans += Amino.Mass[s[i]];
            yield return ans;
        }
        ans = 0.0;
        for (int i = s.Length - 1; i > 0; --i) {
            ans += Amino.Mass[s[i]];
            yield return ans;
        }
    }

    static int MaxCount(string s, IEnumerable<long> r) {
        var cs = CompleteSpectrum(s).Select(k => (long)Math.Round(k * M));
        var md = MinkowskiDifference.Of(r, cs);
        return md.GroupBy(k => k / 10).Max(kv => kv.Count());
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = new string[n];
        for (int i = 0; i < n; ++i) s[i] = Console.ReadLine();
        var r = new List<long>();
        for (;;) {
            var line = Console.ReadLine();
            if (line == null) break;
            r.Add((long)Math.Round(M * double.Parse(line)));
        }
        var arg = s.MaxBy(k => MaxCount(k, r));
        Console.WriteLine(MaxCount(arg, r));
        Console.WriteLine(arg);
    }
}
