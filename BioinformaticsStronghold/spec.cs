using System;
using System.Collections.Generic;
using System.Linq;

class spec {
    static IEnumerable<double> ReadAll() {
        for (;;) {
            var line = Console.ReadLine();
            if (line == null) break;
            yield return double.Parse(line);
        }
    }
    
    public static void Main() {
        var a = new List<double>(ReadAll());
        var ans = Enumerable.Range(0, a.Count - 1).Select(k => {
            var mass = a[k + 1] - a[k];
            return Amino.FromMass[(long)Math.Round(Amino.M * mass)];
        });
        Console.WriteLine(String.Concat(ans));
    }
}
