﻿using System;
using System.Collections.Generic;
using System.Linq;

class fibd {
    static IEnumerable<long> Wabbit(int m) {
        var a = new Queue<long>(m);
        for (int i = 1; i < m; ++i) a.Enqueue(0L);
        a.Enqueue(1L);
        var sum = 1L;
        var last = 1L;
        for (;;) {
            yield return sum;
            var born = sum - last;
            a.Enqueue(born);
            last = born;
            sum = sum + born - a.Dequeue();
        }
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        Console.WriteLine(Wabbit(m).ElementAt(n - 1));
    }
}
