﻿using System;

class revc {
    public static void Main() {
        var s = Console.ReadLine().ToCharArray();
        Array.Reverse(s);
        for (int i = 0; i < s.Length; ++i)
            switch (s[i]) {
                case 'A': s[i] = 'T'; break;
                case 'C': s[i] = 'G'; break;
                case 'G': s[i] = 'C'; break;
                case 'T': s[i] = 'A'; break;
            }
        Console.WriteLine(new string(s));
    }
}
