﻿using System;
using System.Collections.Generic;
using System.Linq;

class lrep {
    class SuffixTreeNode {
        public List<SuffixTreeNode> Childs = new List<SuffixTreeNode>();
        public int Start, Length;
    }

    class SuffixTree {
        public string S;
        public SuffixTreeNode Root;
    }

    class SuffixTreeReader {
        Dictionary<string, SuffixTreeNode> ByName = 
            new Dictionary<string, SuffixTreeNode>();
        
        SuffixTreeNode GetNodeByName(string name) {
            if (ByName.TryGetValue(name, out var node)) return node;
            var newNode = new SuffixTreeNode();
            ByName.Add(name, newNode);
            return newNode;
        }

        public SuffixTree Read(string s) {
            for (;;) {
                var line = Console.ReadLine();
                if (line == null) break;
                var lineS = line.Split();
                var parent = GetNodeByName(lineS[0]);
                var child = GetNodeByName(lineS[1]);
                child.Start = int.Parse(lineS[2]) - 1;
                child.Length = int.Parse(lineS[3]);
                parent.Childs.Add(child);
            }
            return new SuffixTree() {
                S = s, Root = ByName.Values.First(k => k.Length == 0)
            };
        }
    }

    class LongestSubstringK {
        public SuffixTree Tree;
        public int K;
        public string Result = "";

        int Visit(SuffixTreeNode v, string s) {
            int size = v.Childs.Count == 0 ? 1 : 0;
            foreach (var i in v.Childs) {
                size += Visit(i, s + Tree.S.Substring(i.Start, i.Length));
            }
            if (size >= K && s.Length > Result.Length) Result = s;
            return size;
        }

        public LongestSubstringK(SuffixTree tree, int k) {
            Tree = tree;
            K = k;
            Visit(Tree.Root, "");
        }
    }

    public static void Main() {
        var s = Console.ReadLine();
        var k = int.Parse(Console.ReadLine());
        var tree = new SuffixTreeReader().Read(s);
        Console.WriteLine(new LongestSubstringK(tree, k).Result);
    }
}
