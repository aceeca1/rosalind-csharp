using System;

class trie {
    class TrieTraverse {
        public Trie Tr;
        int no = 0;
        
        public TrieTraverse(Trie tr) { 
            Tr = tr;
            Visit(Tr.Root, 0, default(char));
        }
        
        void Visit(Trie.Node v, int noParent, char c) {
            int num = ++no;
            if (noParent != 0) Console.WriteLine($"{noParent} {num} {c}");
            for (int i = 0; i < 4; ++i)
                if (v.Childs[i] != null) Visit(v.Childs[i], num, "ACGT"[i]);
        }
    }
    
    public static void Main() {
        var tr = new Trie();
        for (;;) {
            var line = Console.ReadLine();
            if (line == null) break;
            tr.Add(line);
        }
        new TrieTraverse(tr);
    }
}
