﻿using System;
using System.Collections.Generic;
using System.Linq;

class grph {
    public static void Main() {
        var fasta = new List<Fasta>(Fasta.Parse(Console.In));
        foreach (var i in Enumerable.Join(
            fasta, fasta,
            k => k.Content.Substring(k.Content.Length - 3),
            k => k.Content.Substring(0, 3),
            (k1, k2) => object.ReferenceEquals(k1, k2) 
                ? null 
                : $"{k1.Name} {k2.Name}"
        )) if (i != null) Console.WriteLine(i); 
    }
}
