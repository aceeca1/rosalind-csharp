﻿using System;
using System.Linq;

class eval {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var s = Console.ReadLine();
        var numCG = s.Count(k => k == 'G' || k == 'C');
        var numAT = s.Length - numCG;
        var ans = Console.ReadLine().Split().Select(k => {
            var p = double.Parse(k);
            var p1 = Math.Pow(0.5 * p, numCG);
            var p2 = Math.Pow(0.5 * (1.0 - p), numAT);
            var times = n - s.Length + 1;
            return p1 * p2 * times;
        });
        Console.WriteLine(String.Join(" ", ans));
    }
}
