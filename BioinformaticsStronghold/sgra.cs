using System;
using System.Collections.Generic;

class sgra {
    public static void Main() {
        var a = new List<double>();
        for (;;) {
            var line = Console.ReadLine();
            if (line == null) break;
            a.Add(double.Parse(line));
        }
        var b = new int[a.Count];
        var bFrom = new int[a.Count];
        var bResult = new char[a.Count];
        var arg = 0;
        for (int i = 1; i < a.Count; ++i) {
            for (int j = 0; j < i; ++j) {
                var delta = (long)Math.Round(Amino.M * (a[i] - a[j]));
                if (Amino.FromMass.TryGetValue(delta, out char c)) 
                    if (b[j] + 1 > b[i]) {
                        b[i] = b[j] + 1;
                        bFrom[i] = j;
                        bResult[i] = c;
                    }
            }
            if (b[i] > b[arg]) arg = i;
        }
        var ans = new char[b[arg]];
        for (int i = ans.Length - 1; i >= 0; --i) {
            ans[i] = bResult[arg];
            arg = bFrom[arg];
        }
        Console.WriteLine(new string(ans));
    }
}
