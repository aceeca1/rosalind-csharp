using System;
using System.Collections.Generic;

class full {
    static List<double> A = new List<double>();
    static char[] Result;
    static string ResultS;
    
    static void put(int aS, int aT, int rS, int rT, int iS, int iT) {
        if (iS < 0) { 
            ResultS = new string(Result, rS, rT - rS + 1);
            throw new Success();
        }
        var deltaS = (long)Math.Round(Amino.M * (A[aS] - A[iS]));
        if (Amino.FromMass.TryGetValue(deltaS, out char cS)) {
            Result[rS - 1] = cS;
            put(iS, aT, rS - 1, rT, iS - 1, iT + 1);
        }
        var deltaT = (long)Math.Round(Amino.M * (A[iT] - A[aT]));
        if (Amino.FromMass.TryGetValue(deltaT, out char cT)) {
            Result[rT + 1] = cT;
            put(aS, iT, rS, rT + 1, iS - 1, iT + 1);
        }
    }
    
    public static void Main() {
        Console.ReadLine();
        for (;;) {
            var line = Console.ReadLine();
            if (line == null) break;
            A.Add(double.Parse(line));
        }
        A.Sort();
        Result = new char[A.Count];
        int mid = (A.Count >> 1) - 1;
        try {
            put(mid, mid, mid + 1, mid, mid - 1, mid + 2);
        } catch (Success) {}
        Console.WriteLine(ResultS);
    }
}
