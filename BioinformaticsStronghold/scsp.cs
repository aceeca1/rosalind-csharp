﻿using System;

class scsp {
    static string S0 = Console.ReadLine();
    static string S1 = Console.ReadLine();
    static int[,] A = new int[S0.Length, S1.Length];

    public static int GetA(int i0, int i1) {
        if (i0 == -1 || i1 == -1) return ComputeA(ref i0, ref i1, out char _);
        return A[i0, i1];
    }

    public static int ComputeA(ref int p0, ref int p1, out char c) {
        if (p0 == -1) { c = S1[p1--]; return p1 + 2; }
        if (p1 == -1) { c = S0[p0--]; return p0 + 2; }
        if (S0[p0] == S1[p1]) { c = S0[p0]; return GetA(--p0, --p1) + 1; }
        var a0 = GetA(p0, p1 - 1) + 1;
        var a1 = GetA(p0 - 1, p1) + 1;
        if (a0 < a1)  { c = S1[p1--]; return a0; }
        else          { c = S0[p0--]; return a1; }
    }

    public static int ComputeA(int p0, int p1) => 
        ComputeA(ref p0, ref p1, out char _);

    public static void Main() {
        for (int i0 = 0; i0 < S0.Length; ++i0)
            for (int i1 = 0; i1 < S1.Length; ++i1) 
                A[i0, i1] = ComputeA(i0, i1);
        int p0 = S0.Length - 1, p1 = S1.Length - 1;
        var ans = new char[A[p0, p1]];
        for (int i = ans.Length - 1; i >= 0; --i)
            ComputeA(ref p0, ref p1, out ans[i]);
        Console.WriteLine(new string(ans));
    }
}
