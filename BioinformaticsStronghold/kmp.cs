﻿using System;
using System.Linq;

class kmp {
    public static void Main() {
        var s = Fasta.Parse(Console.In).First().Content;
        Console.WriteLine(String.Join(" ", new KMP(s).Next.Skip(1)));
    }
}
