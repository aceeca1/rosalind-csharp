﻿using System;

class prot {
    public static void Main() {
        var s = Console.ReadLine();
        for (int i = 0; i < s.Length; i += 3) {
            var c = Amino.FromCodon(s.Substring(i, 3));
            if (c == 'Z') break;
            Console.Write(c);
        }
        Console.WriteLine();
    }
}
