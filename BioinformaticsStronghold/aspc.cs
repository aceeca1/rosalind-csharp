using System;
using System.Collections.Generic;
using System.Linq;

class aspc {
    static List<int> f(int n) {
        var ret = new List<int> { 1 };
        for (int i = 1; i <= n; ++i) {
            ret.Add(0);
            for (int j = ret.Count - 1; j > 0; --j) 
                ret[j] = (ret[j] + ret[j - 1]) % 1000000;
        }
        return ret;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var m = int.Parse(line[1]);
        Console.WriteLine(f(n).Skip(m).Sum() % 1000000);
    }
}
