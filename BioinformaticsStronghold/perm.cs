﻿using System;

class perm {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        var factorial = 1;
        for (int i = 0; i < n; ++i) {
            a[i] = i + 1;
            factorial *= i + 1;
        }
        Console.WriteLine(factorial);
        for (;;) {
            Console.WriteLine(String.Join(" ", a));
            if (!Permutation.Next(a, (k1, k2) => k1 < k2)) break;
        }
    }
}
