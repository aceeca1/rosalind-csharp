﻿using System;
using System.Linq;

class kmer {
    static int DNANum(char c) {
        switch (c) {
            case 'A': return 0;
            case 'C': return 1;
            case 'G': return 2;
            case 'T': return 3;
            default: throw new ArgumentException();
        }
    }

    public static void Main() {
        var s = Fasta.Parse(Console.In).First().Content;
        var a = new int[4 * 4 * 4 * 4];
        var u = 0;
        for (int i = 0; i < 4; ++i)
            u = (u << 2) + DNANum(s[i]);
        for (int i = 4; i < s.Length; ++i) {
            ++a[u];
            u = ((u << 2) & 255) + DNANum(s[i]); 
        }
        ++a[u];
        Console.WriteLine(String.Join(" ", a));
    }
}
