﻿using System;

class pper {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var k = int.Parse(line[1]);
        int ans = 1;
        for (int i = 0; i < k; ++i) ans = ans * (n - i) % 1000000;
        Console.WriteLine(ans);
    }
}
