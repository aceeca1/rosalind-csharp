﻿using System;
using System.Text;
using System.Linq;

class mrep {
    class TrieTraverse {
        public Trie Tr;
        public string S;
        StringBuilder Sb;

        public TrieTraverse(Trie tr, string s) {
            Tr = tr;
            S = s;
            Sb = new StringBuilder(s.Length);
            Visit(tr.Root);
        }

        int Visit(Trie.Node v) {
            int ret = 0, numChild = 0;
            for (int i = 0; i < 5; ++i)
                if (v.Childs[i] != null) {
                    Sb.Append("ACGT$"[i]);
                    ret |= Visit(v.Childs[i]);
                    ++numChild;
                    Sb.Remove(Sb.Length - 1, 1);
                }
            switch (numChild) {
                case 0:
                    var start = S.Length - Sb.Length;
                    var prev = start != 0 ? Trie.DNANum(S[start - 1]) : 4;
                    return 1 << prev;
                case 1: return ret;
                default:
                    if ((ret & (ret - 1)) != 0 && Sb.Length >= 20)
                        Console.WriteLine(Sb);
                    return ret;
            }
        }
    }

    public static void Main() {
        var s = Console.ReadLine() + "$";
        var tr = new Trie();
        for (int i = 0; i < s.Length; ++i) tr.Add(s.Substring(i));
        new TrieTraverse(tr, s);
    }
}
