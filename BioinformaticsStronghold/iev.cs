﻿using System;

class iev {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = new double[6];
        for (int i = 0; i < 6; ++i) a[i] = double.Parse(line[i]);
        var ans = a[0] + a[1] + a[2] + 0.75 * a[3] + 0.5 * a[4];
        Console.WriteLine(ans + ans);
    }
}
