﻿using System;
using System.Linq;

class revp {
    static char Complement(char c) {
        switch (c) {
            case 'A': return 'T';
            case 'C': return 'G';
            case 'G': return 'C';
            case 'T': return 'A';
            default: throw new ArgumentException();
        }
    }

    public static void Main() {
        var dna = Fasta.Parse(Console.In).First().Content;
        for (int i = 0; i <= (dna.Length - 1) << 1; ++i) {
            int i1 = i >> 1;
            int i2 = i - i1;
            for (;;) {
                if (i1 < 0) break;
                if (i2 >= dna.Length) break;
                if (dna[i1] != Complement(dna[i2])) break;
                int len = i2 - i1 + 1;
                if (4 <= len && len <= 12) {
                    Console.Write(i1 + 1);
                    Console.Write(' ');
                    Console.WriteLine(len);
                }
                --i1; ++i2;
            }
        }
    }
}
