﻿using System;
using System.Collections.Generic;
using System.Linq;

class @long {
    static int CommonLength(string s1, string s2) {
        return new KMP(s2).Process(s1).Last();
    }

    class Graph: Hamilton.IGraph {
        public int Threshold;
        public int[,] A;
        public int SizeV() => A.GetLength(0);
        public bool E(int no1, int no2) => A[no1, no2] >= Threshold;
    }

    public static void Main() {
        var dna = new List<string>(Fasta.Parse(Console.In).Select(k => k.Content));
        var g = new Graph {
            Threshold = (dna[0].Length >> 1) + 1,
            A = new int[dna.Count, dna.Count]
        };
        for (int i = 0; i < dna.Count; ++i)
            for (int j = 0; j < dna.Count; ++j)
                g.A[i, j] = CommonLength(dna[i], dna[j]);
        var h = new Hamilton(g);
        Console.Write(dna[h.Result[0]]);
        for (int i = 1; i < h.Result.Length; ++i) {
            var common = g.A[h.Result[i - 1], h.Result[i]];
            Console.Write(dna[h.Result[i]].Substring(common));
        }
        Console.WriteLine();
    }
}
