﻿using System;

class sset {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        int ans = 1;
        for (int i = 0; i < n; ++i) ans = (ans + ans) % 1000000;
        Console.WriteLine(ans);
    }
}
