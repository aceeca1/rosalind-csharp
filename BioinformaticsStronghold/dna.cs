﻿using System;

class dna {
    public static void Main() {
        var a = new int[4];
        foreach (char i in Console.ReadLine())
            switch (i) {
                case 'A': ++a[0]; break;
                case 'C': ++a[1]; break;
                case 'G': ++a[2]; break;
                case 'T': ++a[3]; break;
            }
        Console.WriteLine(System.String.Join(" ", a));
    }
}