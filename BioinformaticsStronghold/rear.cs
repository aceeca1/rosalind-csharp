﻿using System;
using System.Collections.Generic;
using System.Linq;

class rear {
    static IEnumerable<int> SolveAll() {
        for (;;) {
            var perm1 = ReverseDistance.Node.Read();
            var perm2 = ReverseDistance.Node.Read();
            yield return new ReverseDistance(perm1, perm2).Result;
            if (Console.ReadLine() == null) break;
        }
        
    }

    public static void Main() {
        Console.WriteLine(String.Join(" ", SolveAll()));
    }
}
