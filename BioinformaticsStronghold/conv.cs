using System;
using System.Linq;

class conv {
    const int M = 100000;
    
    static long[] ReadSet() {
        var line = Console.ReadLine().Split();
        var ret = new long[line.Length];
        for (int i = 0; i < ret.Length; ++i) 
            ret[i] = (long)Math.Round(double.Parse(line[i]) * M);
        return ret;
    }
    
    public static void Main() {
        var s1 = ReadSet();
        var s2 = ReadSet();
        var md = MinkowskiDifference.Of(s1, s2);
        var arg = md.GroupBy(k => k).MaxBy(kv => kv.Count());
        Console.WriteLine(arg.Count());
        Console.WriteLine((double)arg.Key / M);
    }
}
