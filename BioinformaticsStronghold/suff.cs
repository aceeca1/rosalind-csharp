﻿using System;
using System.Linq;

class suff {
    class TrieTraverse {
        public Trie Tr;

        public TrieTraverse(Trie tr) {
            Tr = tr;
            Visit(tr.Root, "");
        }

        void Visit(Trie.Node v, string s) {
            if (v.Childs.Count(k => k != null) != 1) {
                if (s.Length != 0) Console.WriteLine(s);
                s = "";
            }
            for (int i = 0; i < 5; ++i)
                if (v.Childs[i] != null)
                    Visit(v.Childs[i], s + "ACGT$"[i]);
        }
    }

    public static void Main() {
        var s = Console.ReadLine();
        var tr = new Trie();
        for (int i = 0; i < s.Length; ++i) tr.Add(s.Substring(i));
        new TrieTraverse(tr);
    }
}
