﻿using System;
using System.Linq;

class rstr {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var x = double.Parse(line[1]);
        var s = Console.ReadLine();
        int numCG = s.Count(k => k == 'C' || k == 'G');
        int numAT = s.Length - numCG;
        var p = Math.Pow(x * 0.5, numCG) * Math.Pow((1 - x) * 0.5, numAT);
        Console.WriteLine(1 - Math.Pow(1 - p, n));
    }
}
