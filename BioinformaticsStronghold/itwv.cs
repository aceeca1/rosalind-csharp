using System;
using System.Collections.Generic;
using System.Linq;

class itwv {
    static bool IsCompose(string s, string s1, string s2) {
        var a = new bool[s1.Length + 1, s2.Length + 1];
        for (int i1 = 0; i1 <= s1.Length; ++i1)
            for (int i2 = 0; i2 <= s2.Length; ++i2) {
                if (i1 == 0 && i2 == 0) a[i1, i2] = true;
                if (i1 != 0 && s1[i1 - 1] == s[i1 + i2 - 1])
                    if (a[i1 - 1, i2]) a[i1, i2] = true;
                if (i2 != 0 && s2[i2 - 1] == s[i1 + i2 - 1])
                    if (a[i1, i2 - 1]) a[i1, i2] = true;
            }
        return a[s1.Length, s2.Length];
    }

    static bool Interwoven(string s, string s1, string s2) {
        var n = s1.Length + s2.Length;
        for (int i = 0; i + n - 1 < s.Length; ++i)
            if (IsCompose(s.Substring(i, n), s1, s2)) return true;
        return false;
    }
    
    public static void Main() {
        var ss = Console.ReadLine();
        var s = new List<string>();
        for (;;) {
            var line = Console.ReadLine();
            if (line == null) break;
            s.Add(line);
        }
        foreach (var i in s) {
            var ans = s.Select(k => Interwoven(ss, i, k) ? 1 : 0);
            Console.WriteLine(String.Join(" ", ans));
        }
    }
}
