﻿using System;
using System.Collections.Generic;
using System.Linq;

class lgis {
    static IEnumerable<int> LongestIncSubseq(int[] a) {
        var b = new int[a.Length + 1];
        for (int i = 0; i < b.Length; ++i) b[i] = int.MaxValue;
        var aFrom = new int[a.Length];
        var bFrom = new int[b.Length];
        b[0] = int.MinValue;
        bFrom[0] = -1;
        for (int i = 0; i < a.Length; ++i) {
            var k = BSearch.Int(0, b.Length, kk => b[kk] > a[i]);
            b[k] = a[i];
            bFrom[k] = i;
            aFrom[i] = bFrom[k - 1];
        }
        var ans = BSearch.Int(0, b.Length, k => b[k] == int.MaxValue) - 1;
        var u = bFrom[ans];
        yield return u;
        for (int i = 1; i < ans; ++i) yield return u = aFrom[u];
    }

    public static void Main() {
        Console.ReadLine();
        var line = Console.ReadLine().Split();
        var a = new int[line.Length];
        var b = new int[line.Length];
        for (int i = 0; i < a.Length; ++i) {
            a[i] = int.Parse(line[i]);
            b[i] = -a[i];
        }
        var ans1 = LongestIncSubseq(a).Reverse().Select(k => a[k]);
        Console.WriteLine(String.Join(" ", ans1));
        var ans2 = LongestIncSubseq(b).Reverse().Select(k => a[k]);
        Console.WriteLine(String.Join(" ", ans2));
    }
}
