﻿using System;
using System.Linq;

class gc {
    static double GCRatio(Fasta dna) {
        double gc = dna.Content.Count(k => k == 'G' || k == 'C');
        return gc / dna.Content.Length;
    } 

    public static void Main() {
        var argGCRatio = Fasta.Parse(System.Console.In).MaxBy(k => GCRatio(k));
        var maxGCRatio = GCRatio(argGCRatio);
        Console.WriteLine(argGCRatio.Name);
        Console.WriteLine(maxGCRatio * 100);
    }
}
