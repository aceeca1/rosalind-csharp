﻿using System;

class edit {
    static int Min(int a1, int a2, int a3) {
        return Math.Min(Math.Min(a1, a2), a3);
    }

    public static void Main() {
        var fasta = Fasta.Parse(Console.In).GetEnumerator();
        fasta.MoveNext();
        var s0 = fasta.Current.Content;
        fasta.MoveNext();
        var s1 = fasta.Current.Content;
        var a = new int[s0.Length, s1.Length];
        for (int i0 = 0; i0 < s0.Length; ++i0)
            for (int i1 = 0; i1 < s1.Length; ++i1) {
                var a1 = i0 != 0 && i1 != 0 ? a[i0 - 1, i1 - 1] : i0 + i1;
                var a2 = i0 != 0 ? a[i0 - 1, i1] : i1 + 1;
                var a3 = i1 != 0 ? a[i0, i1 - 1] : i0 + 1;
                a[i0, i1] = s0[i0] == s1[i1] ? a1 : Min(a1, a2, a3) + 1;
            }
        Console.WriteLine(a[s0.Length - 1, s1.Length - 1]);
    }
}
