﻿using System;
using System.Linq;

class splc {
    public static void Main() {
        var fasta = Fasta.Parse(Console.In);
        var s = fasta.Select(k => k.Content).GetEnumerator();
        s.MoveNext();
        var s0 = s.Current;
        while (s.MoveNext()) s0 = s0.Replace(s.Current, "");
        s0 = s0.Replace('T', 'U');
        for (int i = 0; i + 2 < s0.Length; i += 3) {
            var amino = Amino.FromCodon(s0.Substring(i, 3));
            if (amino == 'Z') break;
            Console.Write(amino);
        }
        Console.WriteLine();
    }
}
