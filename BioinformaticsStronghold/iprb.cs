﻿using System;

class iprb {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var k = double.Parse(line[0]);
        var m = double.Parse(line[1]);
        var n = double.Parse(line[2]);
        var population = k + m + n;
        var allCases = population * (population - 1.0);
        var recessiveCases = 0.25 * m * (m - 1.0) + m * n + n * (n - 1.0);
        var dominantCases = allCases - recessiveCases;
        Console.WriteLine(dominantCases / allCases);
    }
}
