﻿using System;
using System.Text;

class lexv {
    static void PrintTree(string s, int n, StringBuilder sb) {
        if (sb.Length != 0) Console.WriteLine(sb);
        if (sb.Length == n) return;
        foreach (var i in s) {
            sb.Append(i);
            PrintTree(s, n, sb);
            sb.Remove(sb.Length - 1, 1);
        }
    }

    public static void Main() {
        var s = Console.ReadLine().Replace(" ", "");
        var n = int.Parse(Console.ReadLine());
        PrintTree(s, n, new StringBuilder());
    }
}
