﻿using System;
using System.Text;

class lexf {
    static void Print(char[] coll, int n, StringBuilder prefix) {
        if (n == 0) {
            Console.WriteLine(prefix);
            return;
        }
        foreach (var i in coll) {
            prefix.Append(i);
            Print(coll, n - 1, prefix);
            prefix.Remove(prefix.Length - 1, 1);
        }
    }

    public static void Main() {
        var s = Console.ReadLine().Replace(" ", "").ToCharArray();
        Array.Sort(s);
        var n = int.Parse(Console.ReadLine());
        Print(s, n, new StringBuilder());
    }
}
