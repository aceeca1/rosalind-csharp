﻿using System;
using System.Numerics;
using System.Linq;

class mmch {
    static BigInteger Permute(int n, int k) {
        BigInteger ans = 1;
        for (int i = 0; i < k; ++i) ans *= n - i;
        return ans;
    }

    static BigInteger NumMaxMatching(int a1, int a2) {
        if (a1 > a2) return NumMaxMatching(a2, a1);
        return Permute(a2, a1);
    }

    public static void Main() {
        var s = Fasta.Parse(Console.In).First().Content;
        var num = new int[4];
        foreach (var i in s)
            switch (i) {
                case 'A': ++num[0]; break;
                case 'C': ++num[1]; break;
                case 'G': ++num[2]; break;
                case 'U': ++num[3]; break;
            }
        var ans1 = NumMaxMatching(num[0], num[3]);
        var ans2 = NumMaxMatching(num[1], num[2]);
        Console.WriteLine(ans1 * ans2);
    }
}
