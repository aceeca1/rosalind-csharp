﻿using System;
using System.Collections.Generic;
using System.Linq;

class pdst {
    static double PDistance(string s1, string s2) {
        return (double)Hamming.Distance(s1, s2) / s1.Length;
    }

    public static void Main() {
        var dna = new List<string>(Fasta.Parse(Console.In).Select(k => k.Content));
        foreach (var i in dna)
            Console.WriteLine(String.Join(" ", dna.Select(k => PDistance(i, k))));
    }
}
