﻿using System;

class sign {
    static void PrintSign(int[] a, int m) {
        if (m == a.Length) {
            Console.WriteLine(String.Join(" ", a));
            return;
        }
        PrintSign(a, m + 1);
        a[m] = -a[m];
        PrintSign(a, m + 1);
        a[m] = -a[m];
    }

    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = new int[n];
        var ans = 1 << n;
        for (int i = 0; i < n; ++i) {
            a[i] = i + 1;
            ans *= i + 1;
        }
        Console.WriteLine(ans);
        for (;;) {
            PrintSign(a, 0);
            if (!Permutation.Next(a, (k1, k2) => k1 < k2)) break;
        }
    }
}
