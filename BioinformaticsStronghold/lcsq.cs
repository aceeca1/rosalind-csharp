﻿using System;

class lcsq {
    public static void Main() {
        var fasta = Fasta.Parse(Console.In).GetEnumerator();
        fasta.MoveNext();
        var s0 = fasta.Current.Content;
        fasta.MoveNext();
        var s1 = fasta.Current.Content;
        var a = new int[s0.Length, s1.Length];
        for (int i0 = 0; i0 < s0.Length; ++i0)
            for (int i1 = 0; i1 < s1.Length; ++i1) {
                var a1 = i0 != 0 && i1 != 0 ? a[i0 - 1, i1 - 1] : 0;
                var a2 = i0 != 0 ? a[i0 - 1, i1] : 0;
                var a3 = i1 != 0 ? a[i0, i1 - 1] : 0;
                a[i0, i1] = s0[i0] == s1[i1] ? a1 + 1 : Math.Max(a2, a3);
            }
        int p0 = s0.Length - 1, p1 = s1.Length - 1;
        var ans = new char[a[p0, p1]];
        for (;;) {
            var a1 = p0 != 0 && p1 != 0 ? a[p0 - 1, p1 - 1] : 0;
            var a2 = p0 != 0 ? a[p0 - 1, p1] : 0;
            var a3 = p1 != 0 ? a[p0, p1 - 1] : 0;
            if (s0[p0] == s1[p1]) {
                ans[a[p0, p1] - 1] = s0[p0];
                if (a[p0--, p1--] == 1) break;
            } else if (a2 > a3) --p0; else --p1;
        }
        Console.WriteLine(new string(ans));
    }
}
