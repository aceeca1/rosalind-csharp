﻿using System;
using System.Linq;

class sseq {
    public static void Main() {
        var fasta = Fasta.Parse(Console.In);
        var s = fasta.Select(kk => kk.Content).GetEnumerator();
        s.MoveNext();
        var s0 = s.Current;
        s.MoveNext();
        var s1 = s.Current;
        var k = 0;
        var ans = Enumerable.Range(0, s1.Length).Select(i => {
            while (s0[k] != s1[i]) ++k;
            return ++k;
        });
        Console.WriteLine(String.Join(" ", ans));
    }
}
