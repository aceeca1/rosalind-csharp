﻿using System;
using System.Collections.Generic;

class lia {
    static List<double> f(int m) {
        var ret = new List<double>();
        ret.Add(1.0);
        for (int i = 0; i < m; ++i) {
            ret.Add(0.0);
            for (int j = ret.Count - 1; j >= 0; --j) {
                var p1 = j == 0 ? 1.0 : ret[j - 1];
                var p2 = ret[j];
                ret[j] = 0.25 * p1 + 0.75 * p2;
            }
        }
        return ret;
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var k = int.Parse(line[0]);
        var n = int.Parse(line[1]);
        Console.WriteLine(f(1 << k)[n]);
    }
}
