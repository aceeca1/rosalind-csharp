﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

class orf {
    public static void Main() {
        var reProtein = new Regex("(?=(M[^Z]*)Z)");
        var dna = Fasta.Parse(Console.In).First().Content;
        var rna = dna.Replace('T', 'U');
        var rnaRC = ReverseComplement.Of("ACGU", rna);
        var u = new HashSet<string>();
        foreach (var i1 in new string[] { rna, rnaRC }) {
            for (int i2 = 0; i2 < 3; ++i2) {
                var s = new StringBuilder();
                for (int i3 = i2; i3 + 2 < i1.Length; i3 += 3)
                    s.Append(Amino.FromCodon(i1.Substring(i3, 3)));
                foreach (Match i3 in reProtein.Matches(s.ToString()))
                    u.Add(i3.Groups[1].ToString());
            }
        }
        foreach (var i in u) Console.WriteLine(i);
    }
}
