﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

class mprt {
    public static void Main() {
        var nGlycosylation = new Regex("(?=N[^P][ST][^P])");
        for (;;) {
            var line = Console.ReadLine();
            if (line == null) break;
            var protein = Uniprot.Get(line).First().Content;
            var matches = nGlycosylation.Matches(protein).Cast<Match>();
            var ans = String.Join(" ", matches.Select(k => k.Index + 1));
            if (ans.Length != 0) {
                Console.WriteLine(line);
                Console.WriteLine(ans);
            }
        }
    }
}
