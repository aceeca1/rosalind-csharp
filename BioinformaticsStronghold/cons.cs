﻿using System;
using System.Linq;
using System.Collections.Generic;

class cons {
    public static void Main() {
        var fasta = new List<Fasta>(Fasta.Parse(Console.In));
        int n = fasta[0].Content.Length;
        var a = new int[n, 4];
        for (int i = 0; i < n; ++i) {
            foreach (var j in fasta)
                switch (j.Content[i]) {
                    case 'A': ++a[i, 0]; break;
                    case 'C': ++a[i, 1]; break;
                    case 'G': ++a[i, 2]; break;
                    case 'T': ++a[i, 3]; break;
                }
            var arg = Enumerable.Range(0, 4).MaxBy(k => a[i, k]);
            Console.Write("ACGT"[arg]);
        }
        Console.WriteLine();
        for (int i = 0; i < 4; ++i) {
            Console.Write("ACGT"[i]);
            Console.Write(": ");
            var ans = Enumerable.Range(0, n).Select(k => a[k, i]);
            Console.WriteLine(String.Join(" ", ans));
        }
    }
}
