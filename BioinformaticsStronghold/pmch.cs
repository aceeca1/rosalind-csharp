﻿using System;
using System.Numerics;
using System.Linq;

class pmch {
    static BigInteger Factorial(int n) {
        BigInteger ans = 1;
        for (int i = n; i > 1; --i) ans *= i;
        return ans;
    }

    public static void Main() {
        var s = Fasta.Parse(Console.In).First().Content;
        var num = new int[4];
        foreach (var i in s) 
            switch (i) {
                case 'A': ++num[0]; break; 
                case 'C': ++num[1]; break;
                case 'G': ++num[2]; break;
                case 'U': ++num[3]; break;
            }
        Console.WriteLine(Factorial(num[0]) * Factorial(num[1]));
    }
}
