﻿using System;
using System.Linq;

class tran {
    static char Transition(char c) {
        switch (c) {
            case 'A': return 'G';
            case 'C': return 'T';
            case 'G': return 'A';
            case 'T': return 'C';
            default: throw new ArgumentException();
        }
    }

    public static void Main() {
        var fasta = Fasta.Parse(Console.In);
        var s = fasta.Select(k => k.Content).GetEnumerator();
        s.MoveNext();
        var s0 = s.Current;
        s.MoveNext();
        var s1 = s.Current;
        var numTransition = 0;
        var numTransversion = 0;
        for (int i = 0; i < s1.Length; ++i)
            if (Transition(s0[i]) == s1[i]) ++numTransition;
            else if (s0[i] != s1[i]) ++numTransversion;
        Console.WriteLine((double)numTransition / numTransversion);
    }
}
