﻿using System;
using System.Collections.Generic;
using System.Linq;

class seto {
    static HashSet<int> ParseIntSet(string s) =>
        new HashSet<int>(s.Split(
            new string[] { "{", ", ", "}" },
            StringSplitOptions.RemoveEmptyEntries
        ).Select(k => int.Parse(k)));

    static string StringFromIntSet(IEnumerable<int> ie) => 
        $"{{{ String.Join(", ", ie) }}}";
    
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var a = ParseIntSet(Console.ReadLine());
        var b = ParseIntSet(Console.ReadLine());
        Console.WriteLine(StringFromIntSet(a.Union(b)));
        Console.WriteLine(StringFromIntSet(a.Intersect(b)));
        Console.WriteLine(StringFromIntSet(a.Except(b)));
        Console.WriteLine(StringFromIntSet(b.Except(a)));
        var all = Enumerable.Range(1, n);
        Console.WriteLine(StringFromIntSet(all.Except(a)));
        Console.WriteLine(StringFromIntSet(all.Except(b)));
    }
}
