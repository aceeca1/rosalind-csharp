﻿using System;
using System.Collections.Generic;
using System.Linq;

class corr {
    public static void Main() {
        var dna = new List<string>(Fasta.Parse(Console.In).Select(k => k.Content));
        var a = new Dictionary<string, int>();
        foreach (var i in dna)
            foreach (var j in new string[] { i, ReverseComplement.Of("ACGT", i)}) {
                a.TryGetValue(j, out int z);
                a[j] = z + 1;
            }
        var b = new HashSet<string>();
        foreach (var i in a) if (i.Value >= 2) b.Add(i.Key);
        a.Clear();
        foreach (var i in dna) {
            if (b.Contains(i)) continue;
            var c = b.First(k => Hamming.Distance(i, k) == 1);
            Console.WriteLine($"{i}->{c}");
        }
    }
}
