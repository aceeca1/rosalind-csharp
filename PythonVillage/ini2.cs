﻿using System;

class ini2 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        Console.WriteLine(a * a + b * b);
    }
}
