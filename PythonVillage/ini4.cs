﻿using System;
using System.Linq;

class ini4 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a = int.Parse(line[0]);
        var b = int.Parse(line[1]);
        var range = Enumerable.Range(a, b + 1 - a);
        var sum = range.Sum(k => (k & 1) != 0 ? k : 0);
        Console.WriteLine(sum);
    }
}
