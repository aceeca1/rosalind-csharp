﻿using System;
using System.Collections.Generic;

class ini6 {
    public static void Main() {
        var a = new Dictionary<string, int>();
        foreach (var i in Console.ReadLine().Split()) {
            a.TryGetValue(i, out int v);
            a[i] = v + 1;
        }
        foreach (var i in a) Console.WriteLine($"{i.Key} {i.Value}");
    }
}
