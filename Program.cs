﻿using System;

class Program {
    public static void Main() {
        var s = Console.ReadLine().Split('+');
        switch (s.Length) {
            case 1:
                Type.GetType(s[0]).GetMethod("Main").Invoke(null, null);
                break;
            case 2:
                Console.SetIn(new System.IO.StreamReader(s[1]));
                Console.SetOut(new System.IO.StreamWriter(s[1] + ".out"));
                Type.GetType(s[0]).GetMethod("Main").Invoke(null, null);
                Console.In.Close();
                Console.Out.Close();
                break;
        }
    }
}
